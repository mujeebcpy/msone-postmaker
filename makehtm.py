import bs4
import urllib3
import json
import certifi
import sys

filename = sys.argv[1]
with open(filename) as infile_object :
    lines = infile_object.read().splitlines()

moviedata = {}
for idx, part in enumerate(lines):
    
    if "പരിഭാഷ" in part:
        key, value = part.split(':')
        moviedata['translator'] = value
        moviedata['transfb'] = lines[idx+1]
    
    if "_ഭാഷ" in part:
        key, value = part.split(':')
        moviedata['language'] = value
    if "www.imdb.com/title" in part:
        moviedata['imdblink'] = part
    if "പോസ്റ്റർ" in part:
        key, value = part.split(':')
        moviedata['poster'] = value
        moviedata['posterfb'] = lines[idx+1]
       
    if "synopsis" in part:
        syn_frst_line = idx+1
 
  
 
  
print (moviedata)  
release_number = lines[0].split('-')[2]
imdb_url = moviedata['imdblink']
language_of_film = moviedata["language"]
translator_name = moviedata["translator"]
translator_link = moviedata['transfb']
poster_name = moviedata["poster"]
poster_link = moviedata['posterfb']
#synopsis = moviedata['synopsis']
synopsis = ""
for line in lines[syn_frst_line:len(lines)]:
    synopsis = synopsis+'\n'+line
# scrap details from imdb
baseimdb = 'https://www.imdb.com'
https= urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())
html = https.request('GET',imdb_url)
soup = bs4.BeautifulSoup(html.data, 'html.parser')
title = soup.find("title").string
imdb_rating = soup.find('span',{'itemprop':'ratingValue'}).string+'/10'
dir_name = soup.find('div', {'class':'credit_summary_item'}).findChild('a').string
dirname_link = baseimdb+soup.find('div', {'class':'credit_summary_item'}).findChild('a',href=True).get('href')
gener_tag = soup.find('div', {'class':'subtext'}).findChildren('a')
    
gener_list = []
for a in gener_tag:
    gener_list.append(a.string)
gener_list.pop()
print (gener_list)

with open('gener.json') as f:
  data = json.load(f)
if len(gener_list) == 1:
    gener1 = data[gener_list[0]]
if len(gener_list) == 2:
    gener1,gener2 = data[gener_list[0]],data[gener_list[1]]
if len(gener_list) >= 3:
    gener1,gener2,gener3 = data[gener_list[0]],data[gener_list[1]],data[gener_list[2]]          


# load the file
with open("basichtml.html") as inf:
    txt = inf.read()
    soup = bs4.BeautifulSoup(txt, 'html.parser')

soup.find(text="release_number").replace_with(release_number)
soup.find(text="language_of_film").replace_with(language_of_film)
soup.find(text="translator_name").replace_with(translator_name)
soup.find(text="synopsis").replace_with(synopsis)
soup.find(text="poster_name").replace_with(poster_name)
soup.find(text="imdb_rating").replace_with(imdb_rating)
soup.find(text="dir_name").replace_with(dir_name)
if len(gener_list) == 1:
    soup.find(text="gener").replace_with(gener1)
if len(gener_list) == 2:
    soup.find(text="gener").replace_with(gener1+', '+gener2)
if len(gener_list) >= 3:
    soup.find(text="gener").replace_with(gener1+', '+gener2+', '+gener3)          

#soup.find(text="gener2").replace_with(gener2)
#soup.find(text="gener3").replace_with(gener3)

#soup.find(text="dirname_link").replace_with(dirname_link)
for a in soup.findAll('a'):
    a['href'] = a['href'].replace("poster_link", poster_link)
    a['href'] = a['href'].replace("dirname_link", dirname_link)
    a['href'] = a['href'].replace("translator_link", translator_link)
    a['href'] = a['href'].replace("imdb_link", imdb_url)

with open("output_code.html", "w") as outf:
    outf.write(str(soup))
